Decision6 API
=========

A small library providing access to the Decision6 API.

## Installation

  npm install decision6 --save

## Usage

Import the module:

  	var decision6 = require('decision6');

Set your credentials:

	var credentials = {
  		email:'test@test.com',
  		password:'test@pass'
	}

Set your API key:

	var api_key = "YOUR_API_KEY";

See sample.js for examples.


## Release History

* 1.0.0 Initial release
* 1.1.0 Added README
* 1.2.0 Changed restify dependency from "devDependencies" to "dependencies"
* 1.3.0 Move sample.js to installation folder.