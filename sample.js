var Indicators = require('decision6');

var credentials = {
  email:'test@test.com',
  password:'test@pass'
}

var api_key = "YOUR_API_KEY";//

function options()
{
  Indicators.options(credentials,function(result){
    console.log(result);
  });
}

function totalVisits()
{
  // Total of visits per store in a period ordered by total of visits
  Indicators.query(credentials, api_key, {
    interval: ['2015-12-01','2015-12-05'],
    indicators:{
      sum:['visits'],
    }
  },function(result){
    console.log(result);
  });
}

function totalVisitsPerStore()
{
  // Total of visits per store in a period ordered by total of visits
  Indicators.query(credentials, api_key, {
    interval: ['2015-12-01','2015-12-05'],
    indicators:{
      sum:['visits'],
    },
    dimensions:['name'],
    order:['visits desc']
  },function(result){
    console.log(result);
  });
}

function averageVisitsPerDayOfWeek()
{
  Indicators.query(credentials, api_key, {
    interval: ['2015-12-01','2015-12-05'],
    indicators:{
      sum:['showcase'],
      avg:['visits'],
      min:['passers']
    },
    dimensions:['dow_name'],
    order:['visits desc']
  },function(result){
    console.log(result);

    console.log(result.data[0].dow_name+" - "+result.data[0].visits);

  });
}

function indicatorsPerStore()
{
  // Total of visits per store in a period ordered by total of visits
  Indicators.query(credentials, api_key, {
    interval: ['2015-12-01','2015-12-05'],
    indicators:{
      sum:['visits','tickets','showcase','passers'],
    },
    dimensions:['name'],
    order:['visits desc']
  },function(result){
    var tx_conversion = [];
    for (var i = 0 ; i < result.data.length ; i++)
    {
      var value = result.data[i].visits / result.data[i].tickets;
      tx_conversion.push({
        store:result.data[i].name,
        value:value
      });
    }
    console.log(tx_conversion);
  });
}

function visitsInRioDeJaneiroInTheMorning()
{
  Indicators.query(credentials, api_key, {
    interval: ['2015-12-01','2015-12-05'],
    indicators:{
      avg:['visits']
    },
    filters:{
      'city.name':['Rio de Janeiro'],
      'shift_name':['Manhã']
    },
    dimensions:['city.name','country.name','shift_name'],
    //order:['visits desc']
  },function(result){
    console.log(result);
  });
}


// options();
// totalVisits();
// totalVisitsPerStore();
// averageVisitsPerDayOfWeek();
// indicatorsPerStore();
// visitsInRioDeJaneiroInTheMorning();

