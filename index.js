

var Restify = require('restify');

// Auth Endpoint
var authEndpoint = 'https://api.decision6.com';
// var authEndpoint = 'https://localhost:3002';

// Indicators API Endpoint
var indicatorsEndpoint = 'https://api.decision6.com';
// var indicatorsEndpoint = 'http://localhost:3001';

var _token = null;

var paths = {
  login:'/security/auth/login',
  query:'/indicators/query',
  options:'/indicators/options',
  // login:'/auth/login',
  // query:'/query',
  // options:'/options'
}

var login = function(credentials,next){
  var client = Restify.createJsonClient({
    //url: endpoint,
    url: authEndpoint,
    rejectUnauthorized: false // In case of SSL without certification
  });
  client.post(paths.login, credentials, function(err, req, res, result)
  {
    if (!res || res.statusCode !== 200)
    {
      console.log("Login Failed");
      if (res){
        console.log('Status Code: '+res.statusCode);
        console.log('Status Message: '+res.statusMessage);
      }
      if (result) { console.log("Result:"); console.log(result); }
      if (err)    { console.log("Error:"); console.log(err); }
    }
    else
    {
      console.log("Login Success");
      next(result.token);
    }
  });
}

var query = function(credentials,api_key,queryObject,next){
  if (!_token) // need to login
  {
    login(credentials,function(token){
      _token = token;
      query(credentials,api_key,queryObject,next);
    })
  }
  else
  {
    var client = Restify.createJsonClient({
      url: indicatorsEndpoint,
      headers: {
        Authorization:'Bearer '+_token,
        apikey:api_key
      },
      rejectUnauthorized: false // In case of SSL without certification
    });

    client.post(paths.query, queryObject, function(err, req, res, result) {
      // Error
      if (!res || res.statusCode !== 200){
        if (res){
          console.log('Status Code: '+res.statusCode);
          console.log('Status Message: '+res.statusMessage);
        }
        if (result) { console.log("Result:"); console.log(result); }
        if (err)    { console.log("Error:"); console.log(err); }
      } else { // Success
        console.log("Query executed");
        next(result);
      }
    });
  }
}

var options = function(credentials,next){
  if (!_token) // need to login
  {
    login(credentials,function(token){
      _token = token;
      options(credentials,next);
    })
  }
  else
  {
    var client = Restify.createJsonClient({
      url: indicatorsEndpoint,
      headers: {Authorization:'Bearer '+_token},
      rejectUnauthorized: false // In case of SSL without certification
    });

    client.get(paths.options, function(err, req, res, result) {
      // Error
      //console.log(res);
      if (!res || res.statusCode !== 200){
        if (res){
          console.log('Status Code: '+res.statusCode);
          console.log('Status Message: '+res.statusMessage);
        }
        if (result) { console.log("Result:"); console.log(result); }
        if (err)    { console.log("Error:"); console.log(err); }
      } else { // Success
        console.log("Query executed");
        next(result);
      }
    });
  }
}

module.exports = {
  login: login,
  query: query,
  options: options
}



